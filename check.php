<?php

require "vendor/autoload.php";

function sanitize($iban)
{
    return preg_replace("/[^a-zA-Z0-9]/", "", $iban);
}

$iban = sanitize($argv[1]);
$originalIban = $argv[1];
$ibanInfo = [];

if(verify_iban($iban) === true) {
     $ibanInfo = [
        'request_format'	 	=> $originalIban,
        'sanitized_format' 		=> $iban,
        'human_format' 			=> iban_to_human_format($iban),

        'country' 				=> iban_get_country_part($iban),
        'country_name' 			=> iban_country_get_country_name(iban_get_country_part($iban)),
        'country_currency' 		=> iban_country_get_currency_iso4217(iban_get_country_part($iban)),

        'bank_name' 			=> iban_country_get_central_bank_name(iban_get_country_part($iban)),
        'bank_url' 				=> iban_country_get_central_bank_url(iban_get_country_part($iban)),
        'bank_part' 			=> iban_get_bank_part($iban),
        'bban_part' 			=> iban_get_bban_part($iban),

        'account' 				=> iban_get_account_part($iban),
        'is_sepa' 				=> iban_country_is_sepa(iban_get_country_part($iban)),

        'branch' 				=> iban_get_branch_part($iban),
        'checksum' 				=> iban_get_checksum_part($iban),
        'national_checksum' 	=> iban_get_nationalchecksum_part($iban),
    ];
}

print_r($ibanInfo);



